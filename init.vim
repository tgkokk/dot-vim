let g:dirname=expand('<sfile>:p:h')

function LoadFile(path)
    execute 'source' g:dirname . '/config/' . a:path
endfunction

lua require('plugins')
call LoadFile('editing.vim')
call LoadFile('keys.vim')
call LoadFile('plugin-settings.vim')

set exrc
