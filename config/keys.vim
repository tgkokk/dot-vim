let mapleader = ","

" jk and kj to exit insert mode
imap jk <Esc>
imap kj <Esc>

" undotree
nnoremap <F5> :UndotreeToggle<cr>

nnoremap 0 ^
nnoremap ^ 0
vnoremap 0 ^
nnoremap ^ 0

nmap <C-P> :FZF<CR>
