" Leader
let mapleader=","

if &compatible
    set nocompatible
endif

" Pleasant editing
syntax on
filetype plugin indent on
set cursorline
set relativenumber
set incsearch hlsearch ignorecase smartcase
set gdefault
set backspace=2
set expandtab tabstop=2 shiftwidth=2

" Use OS X clipboard
set clipboard=unnamedplus

" NERDCommenter
let g:NERDAltDelims_haskell = 1

set colorcolumn=+1
