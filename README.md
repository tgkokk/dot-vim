# tgkokk's vim/neovim configuration

This is my vim/neovim configuration.

## Plugins

See `config/load-plugins.vim`.

## Installation

For vim:

    git clone https://gitlab.com/tgkokk/dot-vim.git ~/.vim

For neovim:

    git clone https://gitlab.com/tgkokk/dot-vim.git ~/.config/neovim

Then run vim/neovim and execute `:PlugInstall`.

## Adding a new plugin
See the [vim-plug readme](https://github.com/junegunn/vim-plug/blob/master/README.md).
